#include <iostream>
#include <stdlib.h>

using namespace std;

double isInRegion(double, double);

int main()
{
    int pointsToCheck = 100;
    int numOfHits = 0;

    cout << "random point coordinates (x, y): " << endl;

    for (int i = 0; i < pointsToCheck; i++)
    {
        double x = (rand() % 21 - 10) / 10.0, y = (rand() % 21 - 10) / 10.0;

        if (isInRegion(x, y) == 1)
        {
            cout << "(in) ";
            numOfHits++;
        }

        else {
            cout << "(out) ";
        }

        cout << x << ", " << y << endl;
    }

    double result = (numOfHits * 1.0 / pointsToCheck) * 100;

    cout << "hit percentage: " << result << "%" << endl;
    return 0;
}

double isInRegion(double x0, double y0)
{
    if ((x0 * x0 + y0 * y0 <= 1 && x0 >= 0)
        || (y0 >= x0 + 1)
        || (y0 <= -x0 - 1))
    {
        return 1;
    }

    else
    {
        return 0;
    }
}